# Grind to Gore

A collection of grind/metalcore to go with the fine ASMR.

## [Aborted](https://www.metal-archives.com/bands/Aborted/213)

- [Источник Болезни (The Origin Of Disease)](https://youtu.be/2_7ZDTU6Em0)
- [Deep Red](https://www.youtube.com/watch?v=ak4oeZwBfJs)

## [Cattle Decapitation](https://www.metal-archives.com/bands/Cattle_Decapitation/2840)

- [Bring Black the Plague](https://www.youtube.com/watch?v=BBy4gn8VBMo)
- [One Day Closer to the End of the World](https://www.youtube.com/watch?v=3QjXtrQKf88)

## [Deranged](https://www.metal-archives.com/bands/Deranged/1036)

- [Reverent Decomposition](https://www.youtube.com/watch?v=GhpCegVlN8M)
- [A Murderous Siege](https://www.youtube.com/watch?v=iso8CzPlJUM)

## [Dying Fetus](https://www.metal-archives.com/bands/Dying_Fetus/45)

- [Subjected To A Beating](https://youtu.be/OZ0_AEvv5PY)
- [Grotesque Impalement](https://www.youtube.com/watch?v=K3rDRsEMay0)

## [Splattered Mermaids](https://www.metal-archives.com/bands/Splattered_Mermaids/44275)

- [Digestive System Collapse](https://youtu.be/c49JUk6weis)
- [Spliced Girls](https://youtu.be/31_PKVuNJqo)

## [Sportlov](https://www.metal-archives.com/bands/Sportlov/2643)

- [Snöbollskrieg](https://youtu.be/fZbrM3OtbQM)
- [Svarta pisten](https://youtu.be/qW1nmz0vk7g)

