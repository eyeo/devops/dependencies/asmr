# Bang the core

A collection of raw Metal Core.

## [August Burns Red](https://en.wikipedia.org/wiki/August_Burns_Red)

- [Marianas Trench](https://www.youtube.com/watch?v=HDrsKRLD89s)

## [Caliban](https://en.wikipedia.org/wiki/Caliban)

- [Paralyzed](https://www.youtube.com/watch?v=xk6zL8597dw)

## [Killswitch Engage](https://en.wikipedia.org/wiki/Killswitch_Engage)

- [My Curse](https://www.youtube.com/watch?v=iPW9AbRMwFU)
- [The End Of Heartache](https://www.youtube.com/watch?v=JiDnB-CrrNs)
- [Time Will Not Remain](https://www.youtube.com/watch?v=9BE3JDSsXVg)

## [Parkway Drive](https://en.wikipedia.org/wiki/Parkway_Drive)

- [Absolute Power](https://www.youtube.com/watch?v=JVfTjBYoCVk)

## [Wage War](https://en.wikipedia.org/wiki/Wage_War)

- [Alive](https://www.youtube.com/watch?v=CGqP5LDa6GU)

## [While She Sleeps](https://en.wikipedia.org/wiki/While_She_Sleeps)

- [Silence Speaks](https://www.youtube.com/watch?v=eH6tqXQNWUA) (feat. Oli Sykes)

## [The Sorrow](https://en.wikipedia.org/wiki/The_Sorrow)

- [Grief Machine](https://www.youtube.com/watch?v=7L8ubNq2vNk)
- [Heart Of A Lion](https://www.youtube.com/watch?v=FnT6jJRWrkE)
