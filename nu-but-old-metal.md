# Nu But Old Metal

A collection of quirky Nu Metal and the like from the 1990s and early 2000s.

## [4Lyn](https://en.wikipedia.org/wiki/4Lyn)

- [Lyn](https://www.youtube.com/watch?v=DRbof4A-mRA)
- [Pearls and Beauty](https://www.youtube.com/watch?v=J3I5Ee3BKho)

## [Crazy Town](https://en.wikipedia.org/wiki/Crazy_Town)

- [Battle Cry](https://www.youtube.com/watch?v=ccbU8zlmda0)
- [Only When I'm Drunk](https://www.youtube.com/watch?v=XoroTFH6Iv4)

## [KoЯn](https://en.wikipedia.org/wiki/Korn)

- [Ball Tongue](https://www.youtube.com/watch?v=XDaQaFx2FU4)
- [Hey Daddy](https://www.youtube.com/watch?v=5Ez17h1EkK8)
- [K@#ø%!](https://www.youtube.com/watch?v=RuzyoMFmLbo)
- [Reclaim My Place](https://www.youtube.com/watch?v=C-o8TNGN4hU)

## [Limp Bizkit](https://en.wikipedia.org/wiki/Limp_Bizkit)

- [Break Stuff](https://www.youtube.com/watch?v=atBi_MfT3LE)
- [Full Nelson](https://www.youtube.com/watch?v=zsX0zHB4LwE)
- [Stuck](https://www.youtube.com/watch?v=1QWLek3lodI)

## [P.O.D](https://en.wikipedia.org/wiki/P.O.D.)

- [Breathe Babylon](https://www.youtube.com/watch?v=3uLs_dKhW7s)
- [Draw The Line](https://www.youtube.com/watch?v=lp9tvovhoaE)
- [Lie Down](https://www.youtube.com/watch?v=p5Rmd1Iy46I)
- [Masterpiece Conspiracy](https://www.youtube.com/watch?v=LsuX2i9WJMg)

## [Rage Against The Machine](https://en.wikipedia.org/wiki/Rage_Against_the_Machine)

- [Ashes in the Fall](https://www.youtube.com/watch?v=WaQPEYuzqro)
- [Fistful of Steel](https://www.youtube.com/watch?v=jg8zR91tgSE)
- [Pistol Grip Pump](https://www.youtube.com/watch?v=2vcb--R4z2k)
- [Without a Face](https://www.youtube.com/watch?v=-P_aPXP8qys)

## [Slipknot](https://en.wikipedia.org/wiki/Slipknot_%28band%29)

- [People = Shit](https://www.youtube.com/watch?v=qqK1FrO3BdM)
- [Surfacing](https://www.youtube.com/watch?v=vq-zaD1poAE)

## [Soulfly](https://en.wikipedia.org/wiki/Soulfly)

- [Bumbklaatt](https://www.youtube.com/watch?v=XLl5SLeBpiI)
- [Jumpdafuckup](https://www.youtube.com/watch?v=JkhaK9ZeVyw)

## [System Of A Down](https://en.wikipedia.org/wiki/System_of_a_Down)

- [Psycho](https://www.youtube.com/watch?v=-n90NojU8O0)
- [War?](https://www.youtube.com/watch?v=tgVWJDL3hoE)
